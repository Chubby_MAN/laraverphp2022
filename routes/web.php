<?php

use App\Http\Controllers\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/welcome', function (){
    return view('welcome');
});

Route::get('/page',function(){
    return view('page', ['name' => 'sandro', 'lastname' => 'tsereteli']);
});

Route::get('/registration', function(){
    return view('registration');
});


Route::get('controller/test1', [RegisterController::class, 'test1']);

Route::get('controller/test2/{id}/{age}', [RegisterController::class, 'test2']);

Route::post('/signup', function(Request $r){
    return $r["firstname"];
});