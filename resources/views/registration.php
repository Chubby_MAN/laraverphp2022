<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <h1>Registration</h1>
    <form action="/signup" method="POST">
        <input type="text" name="firstname" placeholder="firstname"> - Firstname
        <br><br>
        <input type="text" name="lastname" placeholder="lastname"> - Lastname
        <br><br>
        <input type="text" name="email" placeholder="email"> - email
        <br><br>
        <input type="text" name="id" placeholder="idnumber"> - idnumber
        <br><br>
        <button type="submit">Registration</button>
    </form>
    <script src="../js/my-script.js"></script>
</body>
</html>